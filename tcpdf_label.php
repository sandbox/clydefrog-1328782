<?php

/**
* @brief Extends TCPDF class to support mailing label formats
*/
class tcpdf_label extends TCPDF {
  // x,y count of label. Upper left is 1,1; lower right is NX-1, NY-1
  // (see format for NX, NY)
  private $coord_x = 0, $coord_y = 0;

  private $label_format;
	private static $avery_formats = array(
		'5160' => array(
      'paper-size' => 'letter',
      'unit' => 'mm', // unit for all measurements
      'marginLeft' => 1.762, // page margins
      'marginTop' => 10.7,
      'NX' => 3,    // number of labels across page
      'NY' => 10,   // number of labels down page
      'SpaceX' => 3.175,  // space between labels
      'SpaceY' => 0,
      'width' => 66.675,  // width of label
      'height' => 25.4,   // height of label
      'padding' => 3,     // padding around edge of label
      'font-size' => 10,   // default font size
    ),
    '5161' => array(
      'paper-size' => 'letter',
      'unit' => 'mm',
      'marginLeft' => 0.967,
      'marginTop' => 10.7,
      'NX' => 2,
      'NY' => 10,
      'SpaceX' => 3.967,
      'SpaceY' => 0,
      'width' => 101.6,
      'height' => 25.4,
      'padding' => 1,
      'font-size' => 8,
    ),
    '5162'  =>  array(
      'paper-size' => 'letter',
      'unit' => 'mm',
      'marginLeft' => 0.97,
      'marginTop' => 20.224,
      'NX' => 2,
      'NY' => 7,
      'SpaceX' => 4.762,
      'SpaceY' => 0,
      'width' => 100.807,
      'height' => 35.72,
      'padding' => 1,
      'font-size' => 8,
    ),
    '5163'  =>  array(
      'paper-size' => 'letter',
      'unit' => 'mm',
      'marginLeft' => 1.762,
      'marginTop' => 10.7,
      'NX' => 2,
      'NY' => 5,
      'SpaceX' => 3.175,
      'SpaceY' => 0,
      'width' => 101.6,
      'height' => 50.8,
      'padding' => 1,
      'font-size' => 8,
    ),
    '5164'  =>  array(
      'paper-size' => 'letter',
      'unit' => 'in',
      'marginLeft' => 0.148,
      'marginTop' => 0.5,
      'NX' => 2,
      'NY' => 3,
      'SpaceX' => 0.2031,
      'SpaceY' => 0,
      'width' => 4.0,
      'height' => 3.33,
      'padding' => 0.04,
      'font-size' => 12,
    ),
    '8600'  =>  array(
      'paper-size' => 'letter',
      'unit' => 'mm',
      'marginLeft' => 7.1,
      'marginTop' => 19,
      'NX' => 3,
      'NY' => 10,
      'SpaceX' => 9.5,
      'SpaceY' => 3.1,
      'width' => 66.6,
      'height' => 25.4,
      'padding' => 1,
      'font-size' => 8,
    ),
    'L7163' =>  array(
      'paper-size' => 'A4',
      'unit' => 'mm',
      'marginLeft' => 5,
      'marginTop' => 15,
      'NX' => 2,
      'NY' => 7,
      'SpaceX' => 25,
      'SpaceY' => 0,
      'width' => 99.1,
      'height' => 38.1,
      'padding' => 1,
      'font-size' => 9,
    ),
    '3422'  =>  array(
      'paper-size' => 'A4',
      'unit' => 'mm',
      'marginLeft' => 0,
      'marginTop' => 8.5,
      'NX' => 3,
      'NY' => 8,
      'SpaceX' => 0,
      'SpaceY' => 0,
      'width' => 70,
      'height' => 35,
      'padding' => 1,
      'font-size' => 9,
    )
  );

  /**
  * @brief Constructor, intializing PDF and setting label format
  *
  * @param $format Label format. The format can either be an Avery
  *        label number, if supported, or an array specifying a custom
  *        label format. For the latter, see the built-in formats
  *        for the required array syntax.
  *
  * @return No return value
  */
  function __construct($format = '5160') {
    if (is_array($format)) {
      $this->label_format = $format;
    }
    else {
      if (isset(self::$avery_formats[$format])) {
        $this->label_format = self::$avery_formats[$format];
      }
      else {
        $this->Error('Unknown label format: "'. $format .'"');
        return;
      }
    }

    parent::__construct('P', $this->label_format['unit'], $this->label_format['paper-size']);

    // We (try to) handle margins internally, so disable TCPDF margins.
    $this->setMargins(0, 0);
    // We break pages manually when we have printed all labels on a page.
    $this->setAutoPageBreak(FALSE);
    // Default to font size specified in format
    $this->setFontSize($this->label_format['font-size']);
  }

  /**
  * @brief Add a label to the document
  *
  * @param $contents Text to print on the label
  * @param $is_html True if $contents should be interpreted as HTML,
  *        false otherwise.
  *
  * @return No return value
  */
  function addLabel($contents, $is_html = true) {
    // Compute writing position
    $x_space_factor =
      $this->label_format['width'] + $this->label_format['SpaceX'];
    $page_x = $this->label_format['marginLeft']
      + ($this->coord_x * $x_space_factor) + $this->label_format['padding'];

    $y_space_factor =
      $this->label_format['height'] + $this->label_format['SpaceY'];
    $page_y = $this->label_format['marginTop']
      + ($this->coord_y * $y_space_factor) + $this->label_format['padding'];

    // Compute cell size
    $cell_width = $this->label_format['width'] - $this->label_format['padding'];
    $cell_height = $this->label_format['height'] - $this->label_format['padding'];

    // Add text to PDF
    // TODO: enforce proper cell height somehow. Currently these writes
    // are auto-expanding.
    if ($is_html) {
      $this->writeHTMLCell($cell_width, $cell_height, $page_x, $page_y, $contents);
    }
    else {
      $this->SetXY($page_x, $page_y);
      $this->MultiCell($cell_width, $cell_height, $contents);
    }

    // Move internal coordinates, adding a page if necessary
    $this->coord_x++;
    if ($this->coord_x == $this->label_format['NX']) {
      $this->coord_x = 0;
      $this->coord_y++;
      if ($this->coord_y == $this->label_format['NY']) {
        // End of page
        $this->coord_y = 0;
        $this->addPage();
      }
    }
  }

  /**
  * @brief Get options array of built-in formats for Drupal FAPI
  *
  * @return An array whose keys are the format IDs and values are the
  *         human-readable names of the formats.
  */
  static function getAveryFormatOptions() {
    $options = array();
    foreach (self::$avery_formats as $key => $format) {
      $options[$key] = t('Avery @key', array('@key' => $key));
    }
    return $options;
  }

  /**
  * @brief Add PDF code to disable print scaling in viewer
  *
  * @return Return value of parent function
  */
  protected function _putcatalog()
  {
    $ret = parent::_putcatalog();
    $this->_out('/ViewerPreferences <</PrintScaling /None>>');
    return $ret;
  }
}
