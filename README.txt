Mailing Label: README.txt
------------------------

This module prints mailing labels using the TCPDF library. It adds
a "display" to Views 3 and supports multiple Avery label formats.

The label layout code and label dimensions are based on similar code
for FPDF, provided as an example script here:

http://www.fpdf.de/downloads/addons/29/

Dependencies:
-----------
- Views module

Developers:
-----------
- Aaron Couch: http://zivtech.com

- Evan Heidtmann

Instructions
-----------
- Install like any other module.
- Go to the view to which you would like to add mailing labels.
- Add a "Mailing Labels" display.
- Choose an URL for the PDF.
- Choose the label layout by clicking on the "Settings" link next to
  the "Format: Avery Mailing Label" item in the left-hand column.

Known Issues
----------
- Formatting of labels:

  The code for converting HTML into a visual layout for use in the PDF
  is very bad. It's likely to break for anything but the simplest text.
