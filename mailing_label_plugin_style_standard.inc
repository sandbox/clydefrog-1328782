<?php

class mailing_label_plugin_style_standard extends views_plugin_style {
  function render() {

    // Group the rows according to the grouping field, if specified.
    // (calls render_fields(), which is required before row_plugin render)
    $sets = $this->render_grouping($this->view->result, $this->options['grouping']);

    // Render each row and put them in an array.
    $rows = array();
    foreach ($sets as $title => $records) {
      foreach ($records as $row_index => $row) {
        $this->view->row_index = $row_index;
        $rows[$row_index] = $this->row_plugin->render($row);
      }
    }
    unset($this->view->row_index);

    return $rows;
  }

  function option_definition() {
    $options = parent::option_definition();
    unset($options['grouping']); // TODO: support grouping

    // the label format implicitly defines margins, # per page, etc
    $options['label_format'] = array('default' => '5160');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['grouping']['#access'] = false; // TODO: support grouping

    $form['label_format'] = array(
      '#type' => 'select',
      '#title' => t('Label Format'),
      '#required' => TRUE,
      '#options' => mailing_label_get_formats(),
      '#description' => t(''),
      '#default_value' => $this->options['label_format'],
    );
  }

  function options_submit(&$form, &$form_state) {
    $this->options['label_format'] = $form_state['values']['style_options']['label_format'];
  }
}

