<?php

/**
 * Implementation of hook_views_plugins
 */
function mailing_label_views_plugins() {
  return array(
    'module' => 'mailing_label',
    'display' => array(
      'mailing' => array(
        'title' => t('Mailing Labels'),
        'help' => t('Make PDF mailing labels.'),
        'handler' => 'mailing_label_plugin_display',
        'help topic' => 'mailing-labels',
        'uses hook menu' => TRUE,
        'use ajax' => FALSE,
        'use pager' => FALSE,
        'use_more' => FALSE,
        'accept attachments' => FALSE,
      ),
    ),
    'style' => array(
      'mailing_avery' => array(
        'title' => t('Avery Mailing Label'),
        'help' => t('Use a standard Avery mailing label format'),
        'handler' => 'mailing_label_plugin_style_standard',
        'parent' => 'default',
        'uses row plugin' => TRUE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'mailing_label',
      ),
    ),
  );
}
