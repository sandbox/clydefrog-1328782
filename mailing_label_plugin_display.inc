<?php

/**
 * The plugin that handles date navigation attachments.
 *
 * Creates a special attachment for this purpose only.
 */
class mailing_label_plugin_display extends views_plugin_display_page {

  // allow for style plugins
  function get_style_type() { return 'mailing_label'; }

  // disable breadcrumb
  function uses_breadcrumb() { return FALSE; }
  //function has_path() { return TRUE; }

  // TODO: grok this
  function defaultable_sections($section = NULL) {
    if (in_array($section, array('style_options', 'style_plugin'))) {
      return FALSE;
    }
    return parent::defaultable_sections($section);
  }

  // define options for this display
  function option_definition () {
    $options = parent::option_definition();

    // override defaults for existing options
    // style plugin:
    $options['style_plugin']['default'] = 'mailing_avery';

    $options['defaults']['default']['style_plugin'] = FALSE;
    $options['defaults']['default']['style_options'] = FALSE;

    return $options;
  }

  function options_summary(&$categories, &$options) {
    parent::options_summary($categories, $options);

    $categories['page'] = array(
      'title' => t('Mailing Label Settings'),
      'column' => 'second',
      'build' => array(
        '#weight' => -10,
      ),
    );
  }

  function render() {
    return $this->view->style_plugin->render($this->view->result);
  }

  function preview() {
    $rows = $this->view->render();
    if (count($rows)) {
      $output = '<ul>';
      foreach ($rows as $row) {
        $output .= '<li>'. $row .'</li>';
      }
      $output .= '</ul>';
      return $output;
    }
  }

  function execute() {
    $rows = $this->view->render();
    if(count($rows)) {
      $format = $this->view->style_plugin->options['label_format'];

      mailing_label_tcpdf_label($rows, $format);
    }

    return NULL; // disable normal page theming
  }
}
